use std::io;

mod token;
mod finite_state_automata;

fn main()
{

	let tok_data: token::TokenData = token::TokenData::initialize();
	let input = tok_data.tokenize(get_input());

	match input
	{
		Some(val) =>
		{
			println!("{:?}", val);
			println!("{}", finite_state_automata::generate_fsm("fsm.txt").validate_input(val));
		}
		None => ()
	}

}

fn get_input() -> Vec<String>
{
	let mut input = String::new();
	match io::stdin().read_line(&mut input)
	{
		Ok(_go_into_input) => {},
		Err(_no_update) => {},
	}

	let split = input.split_whitespace();
	let mut vec: Vec<String> = Vec::new();
	for s in split
	{
		let temp = s.to_string();
		vec.push(temp);
	}

	vec
}



