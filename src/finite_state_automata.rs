use std::collections::VecDeque;
use crate::token;
use std::fs::{File};
use std::io::{BufRead, BufReader};

#[derive(Debug)]
pub struct FiniteStateAutomata
{
	num_nodes: usize,
	num_alpha: usize,
	valid_finish: Vec<bool>,
	adj_mat: Vec<usize>,
}

impl FiniteStateAutomata
{
	pub fn validate_input(&self, mut tokens: VecDeque<token::Token>) -> bool
	{
		let mut state: usize = 0;
		while tokens.len() != 0
		{
			match tokens.pop_front()
			{
				Some(val) => match val
				{
					token::Token::Target(_val) 		=> state = self.adj_mat[self.num_alpha * state + 0],
					token::Token::Meta(_val) 		=> state = self.adj_mat[self.num_alpha * state + 1],
					token::Token::Mechanic(_val) 	=> state = self.adj_mat[self.num_alpha * state + 2],
				}
				None => (),
			};
		}
		self.valid_finish[state]
	}
}

pub fn generate_fsm(filename: &str) -> FiniteStateAutomata
{
	let f = File::open(filename).unwrap();
	let f = BufReader::new(f);

	let mut adj_mat: Vec<usize> = Vec::new();
	let mut valid_finish: Vec<bool> = Vec::new();

	for line in f.lines()
	{
		match line
		{
			Ok(val) =>
			{
				let split: Vec<&str> = val.split(" ").collect();

				for s in split
				{
					match s.parse::<usize>()
					{
						Ok(val) => adj_mat.push(val - 1),
						Err(_e) => 
						{
							match s.parse::<bool>()
							{
								Ok(val) => valid_finish.push(val),
								Err(_e) => ()
							}
						}
					}
				}
			}
			Err(_e) => ()
		}
	}

	FiniteStateAutomata{num_nodes: valid_finish.len(),
						num_alpha: adj_mat.len() / valid_finish.len(),
						valid_finish: valid_finish,
						adj_mat: adj_mat}
}
