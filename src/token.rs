use std::collections::VecDeque;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
pub enum Token
{
	Target(String),
	Meta(String),
	Mechanic(String),
}

#[derive(Debug)]
pub struct TokenData
{
	word_table: HashMap<String, String>,
}

impl TokenData
{
	pub fn initialize() -> TokenData
	{
		let mut word_table = HashMap::new();
		let f = File::open("word_dict.txt").unwrap();
		let f = BufReader::new(f);

		for line in f.lines()
		{
			let line = line.expect("Unable to read line.");
			let split_line: Vec<&str> = line.split(":").collect();
			word_table.insert(split_line[0].to_string(), split_line[1].to_string());
		}

		TokenData{word_table: word_table}
	}

	pub fn tokenize(&self, raw_vector: Vec<String>) -> Option<VecDeque<Token>>
	{
		let mut token_vector = VecDeque::new();

		for elem in raw_vector
		{
			let val = self.word_table.get(&elem);
			match val
			{
				Some(val) =>
				{
					match val.as_ref()
					{
						"MECHANIC" 	=> token_vector.push_back(Token::Mechanic(elem)),
						"META" 		=> token_vector.push_back(Token::Meta(elem)),
						"TARGET"	=> token_vector.push_back(Token::Target(elem)),
						_			=> return None
					}
				}
				None => return None
			}
		}

		Some(token_vector)
	}
}

